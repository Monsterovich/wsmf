#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void debug(QString, bool print = true);
    void parse_map(QString fileName, bool fulldebug = false);
    
private slots:
    void on_button_clicked();
    
    void on_opendir_clicked();
    
    void on_actionQuit_triggered();
    
    void on_stop_clicked();
    
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
