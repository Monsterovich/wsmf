#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDebug>
#include <QListWidget>
#include <QFile>
#include <QIODevice>
#include <QDirIterator>
#include <QMessageBox>

#include <StormLib.h>

typedef char mychar_t;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stop->setDisabled(true);
    ui->lowercase->setChecked(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void showError(QString error)
{
    QMessageBox msgBox;
    msgBox.setText("Error.");
    msgBox.setInformativeText(error);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();
}

const char *infoFile = "war3map.wts";

void MainWindow::debug(QString str, bool print)
{
    if (!print)
        return;
    ui->debug_list->insertItem(ui->debug_list->count(), str);
    if (str.toLower().contains("found key")) // highlight bydlocode
        ui->debug_list->item(ui->debug_list->count()-1)->setForeground(Qt::red);
}

void MainWindow::on_button_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QApplication::applicationDirPath(), tr("WC3 Maps (*)"));

    if (fileName.isEmpty())
        return;

    if (ui->keywords->text().isEmpty())
    {
        showError("Empty keywords!");
        return;
    }
    ui->debug_list->clear();

    parse_map(fileName, true);
}

bool found_something = false;
void MainWindow::parse_map(QString fileName, bool fulldebug)
{
    HANDLE mpq;
    HANDLE file;

    //fileName = QString("map://") + fileName;
    QString mapName = fileName.split(QDir::separator()).last();
    debug(QString(), fulldebug);
    debug("map name: " + mapName, fulldebug);


    bool ok = SFileOpenArchive((mychar_t*)fileName.toStdString().c_str(), 0, 0, &mpq);

    debug("get error code for mpq open: " + QString::number(GetLastError()), 
          fulldebug && (GetLastError() != ERROR_HANDLE_EOF || GetLastError() != 0));

    if (!ok)
        return;

    if(!SFileHasFile(mpq, infoFile))
    {
        debug(QString(infoFile) + " is not found in " + fileName, 
              fulldebug && (GetLastError() != ERROR_HANDLE_EOF || GetLastError() != 0));
        SFileCloseArchive(mpq);
        return;
    }

    ok = SFileOpenFileEx(mpq, infoFile, 0, &file);

    debug("get error code for file open (0 or 1002 = okay): " + QString::number(GetLastError()), 
          fulldebug && (GetLastError() != ERROR_HANDLE_EOF || GetLastError() != 0));

    if (!ok)
    {
        SFileCloseArchive(mpq);
        return;
    }

    DWORD fsize = SFileGetFileSize(file, NULL);

    debug("file size: " + QString::number(fsize), fulldebug);

    QByteArray array;

    char buffer[0x10000];
    DWORD read;
    while (SFileReadFile(file, buffer, sizeof(buffer), &read, NULL))
        array.append(buffer, read);

    if (GetLastError() != ERROR_HANDLE_EOF)
    {
        debug("get error code for file read: " + QString::number(GetLastError()), fulldebug);
    }
    array.append(buffer, read);
    debug("bytes total read: " + QString::number(array.size()), fulldebug);

    debug("----------------------", fulldebug);
    QStringList keywords = ui->keywords->text().split(",");
    QTextStream in(array);
    bool found = false;
    while (!in.atEnd())
    {
       QString line = in.readLine();
       if (ui->lowercase->isChecked())
           line = line.toLower();
       for (const QString &key : keywords)
       {
           if (line.contains(ui->lowercase->isChecked() ? key.toLower() : key, Qt::CaseInsensitive))
           {
               found = true;
               debug("Found key '" + key + "' in map '" + fileName + "'");
           }
       }
       //debug(line, fulldebug);
    }

    if(file != NULL)
        SFileCloseFile(file);
    if(mpq != NULL)
        SFileCloseArchive(mpq);

    if (!found)
        debug("Nothing found.", fulldebug);
    if (found)
        found_something = true;
}

bool stopped = false;

void MainWindow::on_opendir_clicked()
{
    if (ui->keywords->text().isEmpty())
    {
        showError("Empty keywords!");
        return;
    }
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 QApplication::applicationDirPath(),
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    if (dir.isEmpty())
        return;
    ui->debug_list->clear();
    QDirIterator it(dir, QStringList() << "*.w3x" << "*.w3m", QDir::Files, QDirIterator::Subdirectories);
    int count = 0;
    found_something = false;
    ui->stop->setDisabled(false);
    while (it.hasNext())
    {
       // qDebug() << it.next();
        if (stopped)
        {
            ui->stop->setDisabled(true);
            ui->progress->setText("Process aborted.");
            stopped = false;
            return;
        }
        QString map = it.next();
        parse_map(map, ui->debug->isChecked());
        count++;
        qApp->processEvents();
        ui->progress->setText("Processed " + QString::number(count) + " maps. " + map);
    }
    const QString output = "output.txt";
    if (QFile::exists(output))
    {
        QFile::copy(output, output + ".bak");
        QFile::remove(output);
    }
    ui->progress->setText("Processed " + QString::number(count) + " maps. Results (if there any) are saved to '" + output + "'");
    ui->stop->setDisabled(true);
    stopped = false;

    if (!found_something)
    {
        debug("No results.");
        return;
    }

    QFile out(output);
    out.open(QIODevice::Append);
    for(int row = 0; row < ui->debug_list->count(); row++)
    {
             QListWidgetItem *item = ui->debug_list->item(row);
             out.write(QString(item->text() + "\n").toUtf8());
    }
    out.close();
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}

void MainWindow::on_stop_clicked()
{
    stopped = true;
}
